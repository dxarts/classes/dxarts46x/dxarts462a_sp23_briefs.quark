/*
Title: Midterm

Course: DXARTS 462a Spring 2023 (https://canvas.uw.edu/courses/1633437)
Authors: Joseph Anderson, Dan Peterson, James Wenlock, et al.
Affiliation: DXARTS, University of Washington (https://dxarts.washington.edu/)
License: GPLv3

*/

/*
Using the following template -> myLastName-midterm.zip, create three 30" - 1' etudes exploring digital signal processing techniques, sound worlds and compositional ideas found (exclusively!) in the literature we have reviewed. Identify the kinds of techniques and transformations that happen in these pieces, and then model these in your short studies. One option could be to choose a particular compositional idea and explore that in three different ways for each of your etudes. Another possibility could be to explore variants of a limited range signal processing of techniques (exploring parameter space). Still another possibility could be to seek to create variations on the material found in a single moment of one of our class repertoire examples. You'll need to state which pieces you're using as your pastiche examples in your write up.

The techniques we have covered so far are:

sampled sound & time-segment processing
digital filtering (except delay lines; e.g. don't use large time-scale delays, do use comb filtering)
You're expected to included at least one example of each of these.

Further advice, your work will be reviewed in three areas:

SuperCollider3 language proficiency: demonstrate competence and creativity through your use of sc-lang
musical signal processing: develop and explore creative signal processing designs
musical language & composition: illustrate your understanding of the repertoire through pastiche composition and mastery of techniques reviewed
NOTE:

Your piece should not:
use techniques (UGens) we have not reviewed
Additionally your midterm pieces should:
creatively explore the techniques we've encountered this Quarter
creatively explore compositional approaches we've reviewed in the literature
show what you've learned
You MAY NOT include:
externally recorded audio files
use only recordings from our Week 1 recording session (avoid including voice)


Specific Requirements:
- 3 etudes 30-60 seconds long
- 3 visual scores (PDFs)
- 3 100 word commentaries on on each etude discussing synthesis techniques and references to the repertoire (in commentaries.scd)

Please submit your project as a zip file of the complete project folder named using the following syntax

[myLastName]-midterm.zip


Midterm How-To
--------------

Below is the midterm project file hierarchy.

myLastName-midterm/

____commentaries.scd <-- commentaries (100 words) on each etude

____functions/

________DO-NOT-EDIT/

________etudes/ <-- where your etudes are composed

____________etude-1.scd

____________etude-2.scd

____________etude-3.scd

________myFunctions/ <-- functions of your design

____graphicScores/ <-- visual scores for each etude

____myLastName-midterm.scd

____README.scd

____soundIn/ <-- Insert your parsed recordings here

____soundOut/

____uGenGraphs/

________DO-NOT-EDIT/

________myUGens/ <-- uGens of your design

You will be writing code in 3 directories

myFunctions/ <-- folder containing functions of your design

etudes/ <-- functions that execute your etudes

myUGens/ <-- uGens that you have modified

In addition to this, place 3 pdfs with visual scores in the graphicScores/ directory and 100 word commentaries on on each etude discussing synthesis techniques and references to the repertoire as comments in the commentaries.scd file. You can find a template for your score inside of graphicScores/


This structure works identically to lecture code and previous assignments where functions and uGens are accessed and executed using main.

Your etudes will be written in the .scd files inside of the etudes/ directory. Like previous assignments, you don't need to worry about creating or rendering the score in these files.

The myUGens/ and myFunctions/ directories are designated for code you write and lecture code you copy. Inside you will find an empty template that you can copy and rename if you'd like to create something from scratch.

You might have noticed that there is no lecture material in the DO-NOT-EDIT folders inside of the functions/ and uGenGraphs/ directories. It is up to you to appropriately copy and edit lecture code for use in your projects.

Your etudes are rendered by running the myLastName-midterm.scd file. Please write your full name at the top of this project. There is a variable called myLastName. Please assign this variable a string containing your last name. You will also noticed three booleans: renderEtude1, renderEtude2 and renderEtude3. These booleans indicate whether or not an etude is rendered and can be changed as you work on your midterm.

For sound material please use the recordings found on canvas. Feel free to chop up these files in a DAW such as reaper or audacity. However, refrain from doing any processing on these files in your DAW.