/*
Course: DXARTS 462a Spring 2023 (https://canvas.uw.edu/courses/1633437)
Authors: Joseph Anderson, Dan Peterson, James Wenlock, et al.
Affiliation: DXARTS, University of Washington (https://dxarts.washington.edu/)
License: GPLv3

----------------------
----- Problem 4b -----
----------------------

Modify the following function to indefinitely sustain a given buffer using the playBufConv uGenGraph written in Problem 4a. This will require 4 steps:

1) Add the buffer represented by "soundIn" to the score

2) Create a kernel using the same buffer and the given "skipTime", "q" and "refFreq"

3) Make a note call to playBufConv using your buffer, the kernel, and the other arguments of this function

4) Add this note to the score.

NOTE:
- You can use the makeKernelBuf function to load your kernel.
- The buffer represented by soundIn can be accessed via main.soundIn[soundIn]
- campare the problem-4-correct.wav file to your output to check your answer

HINTS: Hint: See 04b-fir-filters-from-soundfiles.

*/

({ |main, starttime = 0, dur = 10, playBufGain = 0.0, convGain = 0.0, ris = 0.1, dec = 0.1, rCurve = 0, dCurve = 0, soundIn = 0, refFreq = 587.330, q = 10, skipTime = 1, fadeDur = 0.2, sampleRate = 44100|
	var playBuffer, kernelBuffer, thisSynthDef, thisNote;

	// YOUR CODE HERE
	// ---

	// 1) Add soundIn to the score

	// 2) make kernel and add to score via \makeKernelBuf

	// reference synthDef

	///////////////// CREATE SYNTH NOTE CALL //////////////////
	// 3) make note call

	///////////////// POPULATE THE SCORE //////////////////
	// 4) add note to score

	// ---
})
