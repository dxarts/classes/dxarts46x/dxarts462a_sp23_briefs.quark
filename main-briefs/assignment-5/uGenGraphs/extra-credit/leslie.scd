/*
Course: DXARTS 462a Spring 2023 (https://canvas.uw.edu/courses/1633437)
Authors: Joseph Anderson, Dan Peterson, James Wenlock, et al.
Affiliation: DXARTS, University of Washington (https://dxarts.washington.edu/)
License: GPLv3

*/

/*
------------------------------
======= Extra-Credit =========
------------------------------

Implement a stereo input, stereo output Rotary Loudspeaker Effect a la Zolzer. The Zolzer network can be found in the images folder. Use main.functions[\sinVibDelayLength] to calculate the maximum delay time and delaytime. Use the code from frequencySpreader.scd to implement width and panning.

HINTS:
See
- 05a-delay-lines-comb-filters-i
- 06c-Delay-Lines-Vibrato
- 08b-stereo-imaging-transforms
- 08c-frequency-dependent-imaging

*/

({ |main|
	{ |dur, gain = 0.0, ris = 0.01, dec = 0.01, mix = 1.0, rotAngle = 0.0, widthAngle = 30.0, rate = 5.0, minRate = 1.0, ratio = 1.01, maxRatio = 1.01, inBus = 0|
		var bus = 0;      // var to specify output bus: first output
		var numChannels = 2; // stereo bus!
		var amp;          // a few vars for synthesis
		var maxDelayTime, delayTime;
		var sig, out;     // vars assigned to audio signals
		var delay, delayL, delayR;
		var modLFO;
		var ampEnv;       // var for envelope signal


		// the amplitude envelope nested in the UGen that synthesises the envelope
		ampEnv = EnvGen.kr(
			Env.linen(ris, 1.0 - (ris + dec), dec),
			timeScale: dur
		);

		amp = gain.dbamp;

		// read sound in
		sig = In.ar(inBus, numChannels);

		// YOUR CODE HERE
		// ---

		delay = Silent.ar(numChannels);

		// ---
		// out & envelope
		out = amp * ampEnv * delay;

		// out!!
		Out.ar(bus, out)
	}
})
