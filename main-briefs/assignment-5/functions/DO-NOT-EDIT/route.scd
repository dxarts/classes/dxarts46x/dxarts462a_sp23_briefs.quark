/*
Course: DXARTS 462a Spring 2023 (https://canvas.uw.edu/courses/1633437)
Authors: Joseph Anderson, Dan Peterson, James Wenlock, et al.
Affiliation: DXARTS, University of Washington (https://dxarts.washington.edu/)
License: GPLv3

*/

/*

*/

({ |main|
	var sourceGroup;
	var fxGroup;
	var fxBus;

	sourceGroup = CtkGroup();
	fxGroup = CtkGroup(addAction: \tail);
	fxBus = CtkAudio(2);

	[sourceGroup, fxGroup].do({ |thisGroup|
		thisGroup.addTo(main.score);
	});
	[
		\sourceGroup, sourceGroup,
		\fxGroup, fxGroup,
		\fxBus, fxBus
	].asDict
})
