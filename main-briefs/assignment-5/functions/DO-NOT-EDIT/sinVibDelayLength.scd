/*
Course: DXARTS 462a Spring 2023 (https://canvas.uw.edu/courses/1633437)
Authors: Joseph Anderson, Dan Peterson, James Wenlock, et al.
Affiliation: DXARTS, University of Washington (https://dxarts.washington.edu/)
License: GPLv3

function to return delay line length, T, to be traversed by sinusoidal modulator

args:
maxRatio: maximum resampling ratio, a
modRate: modulation rate, f
*/
({ |maxRatio, modRate|
    var delayLength;

    delayLength = (maxRatio - 1) / (pi * modRate);

	// return (in seconds)
    delayLength;
})
