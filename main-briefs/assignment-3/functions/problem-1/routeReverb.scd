/*
Course: DXARTS 462a Spring 2023 (https://canvas.uw.edu/courses/1633437)
Authors: Joseph Anderson, Dan Peterson, James Wenlock, et al.
Affiliation: DXARTS, University of Washington (https://dxarts.washington.edu/)
License: GPLv3

*/

/*
------------------------
------ Problem 1 -------
------------------------

Modify this function to do the following:

1) Make two groups -> a reverb group "reverbGroup", a source group "sourceGroup" and add them to the score. Be sure that the reverbGroup is added after the sourceGroup using the addAction argument.

2) Make one bus -> a stereo reverb bus "reverbBus". (numChans: 2)

NOTE: the source synthDef will need to have a stereo send.

3) Return a dictionary in the following format

[
\sourceGroup, [your source group],
\reverbGroup, [your reverb group],
\reverbBus, [your reverb bus],
].asDict

NOTE: You can specifying the group order with the addAction argument when creating your groups.
I.e, CtkGroup.new(addAction: \tail).

HINT: See 06b. Delay Lines - Private Bussing
*/

({ |main|
	var sourceGroup;
	var reverbGroup;
	var reverbBus;

	// YOUR CODE HERE
	// ---

	// ---
	[
		\sourceGroup, sourceGroup,
		\reverbGroup, reverbGroup,
		\reverbBus, reverbBus
	].asDict
})
