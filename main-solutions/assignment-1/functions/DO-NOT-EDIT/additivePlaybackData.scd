/*
Course: DXARTS 462a Spring 2023 (https://canvas.uw.edu/courses/1633437)
Authors: Joseph Anderson, Dan Peterson, James Wenlock, et al.
Affiliation: DXARTS, University of Washington (https://dxarts.washington.edu/)
License: GPLv3

Returns data to be used for additivePlayback.

You do not need to change this function.
*/

({ |main|
	[
		// names of buffers to be used for playback
		\buffers, [\bell1, \bell2, \bell3, \bell5],

		// rates of buffers
		\rates, [0.5, 1, 2, 4],

		// starting positions for buffer playback
		\startPoses, [0.1, 0.2, 0.05, 0],

		// starttimes for buffer playback
		\starttimes, [0, 5, 10, 15],

		// gains for buffer playback
		\gains, [10, 14, 12, 6],

		// durations for buffer playback
		\durations, [30, 20, 15, 10],

		// rise times for buffer playback
		\rises, [0.5, 0.5, 0.5, 0.5],

		// decay times for buffer playback
		\decs, [0.5, 0.5, 0.5, 0.5],

		// panning for buffer playback
		\panAngles, [-5, 0, 5, 0]
	].asDict;
})
