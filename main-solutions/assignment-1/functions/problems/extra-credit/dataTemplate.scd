/*
Course: DXARTS 462a Spring 2023 (https://canvas.uw.edu/courses/1633437)
Authors: Joseph Anderson, Dan Peterson, James Wenlock, et al.
Affiliation: DXARTS, University of Washington (https://dxarts.washington.edu/)
License: GPLv3

Template for data to be used for additivePlayback.

Feel free to make copies of this file and fill it with your data
*/

({ |main|
	[
		// names of buffers to be used for playback
		\buffers, [],

		// rates of buffers
		\rates, [],

		// starting positions for buffer playback
		\startPoses, [],

		// starttimes for buffer playback
		\starttimes, [],

		// gains for buffer playback
		\gains, [],

		// durations for buffer playback
		\durations, [],

		// rise times for buffer playback
		\rises, [],

		// decay times for buffer playback
		\decs, [],

		// panning for buffer playback
		\panAngles, []
	].asDict;
})
