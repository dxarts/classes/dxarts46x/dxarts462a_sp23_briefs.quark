/*
Title: assignment-4

Course: DXARTS 462a Spring 2023 (https://canvas.uw.edu/courses/1633437)
Authors: Joseph Anderson, Dan Peterson, James Wenlock, et al.
Affiliation: DXARTS, University of Washington (https://dxarts.washington.edu/)
License: GPLv3

*/

/*
------------------------
===== ASSIGNMENT 4 =====
------------------------
Experiments with Reverb and Delay Lines

----------------------------
=====HOW TO DO HOMEWORK=====
----------------------------

Assignments require you to complete functions and synthDefs contained in the given assignment folder. Problems will come in the form of .scd files located in two problems/ directories contained respectively in the functions/ and synthDefs/ directories.


Below is the assignment-4 project file hierarchy. The problems/ directories are emphasized

assignment-4/

____assignment-4.scd

____functions/

________DO-NOT-EDIT/

________problems/ <- PROBLEMS [1, 2, extra-credit] ARE LOCATED HERE

____README.scd

____soundIn/

____soundOut/

____uGenGraphs/

________DO-NOT-EDIT/

________problems/ <- PROBLEMS [3, extra-credit] ARE LOCATED HERE

Please replace myLastName with your last name.

Upon opening a .scd file in the problems/ directory, you will find a comment header with specifications for that problem. Places for student code are labeled with the following syntax

// YOUR CODE HERE
// ---

// ---

Your answer is to be written between the dashed lines.

Some problems will come in the form of a folder rather than an .scd. If this is the case it is expected that you modify each .scd file(s) contained inside of the folder.

You might have notice their are two folders named DO-NOT-EDIT. These directories, if present, contain code otherwise used to evaluate your answers. The files in these folders are not to be modified!

Your homework will be evaluated by running the code block inside of assignment-4.scd.

Appropriate output for each problem section will either be output in the post window or a soundfile, and is specified within the individual problem file.

You can render specific problems by changing the booleans inside of assignment-4.scd

*/
