dxarts462 : Read Me
========================
_DXARTS 462 (Spring 2023): Introduction to digital sound processing (Assignment Briefs)._

&nbsp;

&nbsp;

Installing
==========

Distributed via [DXARTS 462 Tutorials](https://gitlab.com/dxarts/classes/dxarts46x/dxarts462a_sp23_briefs.quark).


Feedback and Bug Reports
========================

Known issues are logged at
[GitLab](https://gitlab.com/dxarts/classes/dxarts46x/dxarts462a_sp23_briefs.quark/-/issues).

&nbsp;

&nbsp;



List of Changes
---------------

Version 23.8.0

* Spring 2023: week 8
  * assignment-3: solution

Version 23.7.0

* Spring 2023: week 7
  * assignment-5: brief
  * final: brief

Version 23.6.0

* Spring 2023: week 6
  * assignment-3: brief

Version 23.5.0

* Spring 2023: week 5
  * assignment-2: solution

Version 23.3.0

* Spring 2023: week 3
  * assignment-1: solution
  * assignment-2: brief
  * midterm: brief

Version 23.1.0

* Spring 2023: week 1


&nbsp;

&nbsp;

Authors
=======

&nbsp;

Copyright Joseph Anderson and the DXARTS Community, 2020-2023.

[Department of Digital Arts and Experimental Media (DXARTS)](https://dxarts.washington.edu/)
University of Washington

&nbsp;


Contributors
------------

Version 23.3.0 - 23.8.0
*  Joseph Anderson (@joslloand)

Version 23.1.0
*  Joseph Anderson (@joslloand)
*  Wei Yang (@wyang03)

Version pre-release
*  Joseph Anderson (@joslloand)
*  James Wenlock (@wenloj)


Contribute
----------

As part of the wider SuperCollider community codebase, contributors are encouraged to observe the published SuperCollider guidelines found [here](https://github.com/supercollider/supercollider#contribute).


License
=======

The [DXARTS 462 Tutorials](https://gitlab.com/dxarts/classes/dxarts46x/dxarts462a_sp23.quark) is free software available under [Version 3 of the GNU General Public License](https://www.gnu.org/licenses/gpl-3.0.en.html). See [LICENSE](LICENSE) for details.
